// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML elements) from the webpage (document)
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// Alternatively, we can use getElement functions to retrieve the elements.
//const txtFirstName = document.getElementById('txt-first-name');

/*
// The "addEventListener" is a function that takes two arguments
// keyup - string identifying the event
// event => {} - function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event) => {
	// The "innerHTML" property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;

});


txtFirstName.addEventListener('keyup', (event) => {
	// The "event.target" contains the element where the event happened
	console.log(event.target); 
	// The "event.target.value" gets the value of the input object
	console.log(event.target.value);
});




txtLastName.addEventListener('keyup', (event)=> {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
})
*/

// event aka e
const submitFullName = (event) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
	
	console.log(event.target);
	console.log(event.target.value);
	
}

txtFirstName.addEventListener('keyup', submitFullName)
txtLastName.addEventListener('keyup', submitFullName)
